﻿using UnityEngine;

public class Vault : MonoBehaviour {

    public static int goldTotal = 5;

    public static int GetGoldTotal() { return goldTotal; }
    public static void SetGoldTotal(int value) { goldTotal += value; }
}
