﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_Menu_LoadScene : MonoBehaviour {

	public void LoadScene(int sceneIndex) {
        SceneManager.LoadScene(sceneIndex);
    }
}
