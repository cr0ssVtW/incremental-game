﻿using UnityEngine;

public class UI_TogglePurchasePanel : MonoBehaviour {
    public GameObject purchasePanel;

    public void SetPanelActive() {
        if (purchasePanel.activeSelf) {
            purchasePanel.SetActive(false);
        } else {
            purchasePanel.SetActive(true);
        }
    }
}
