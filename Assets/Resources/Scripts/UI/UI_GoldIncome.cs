﻿using UnityEngine;
using UnityEngine.UI;

public class UI_GoldIncome : MonoBehaviour {

    public Text ui_Gold;
    public Text ui_Income;
    public Text ui_Delivery;

    // Update is called once per frame
    void Update () {
        ui_Gold.text = "Gold: " + Vault.GetGoldTotal();
        //ui_Income.text = "Income: " + Income.incomeValue;
        //ui_Delivery.text = "Next Delivery: " + (int)Income.timeRemaining;
    }
}
