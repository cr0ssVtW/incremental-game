﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseController : MonoBehaviour {

    public GameObject[] courierPrefabs;
    /*
    public enum CourierTypes
    {
        CART, 0
        PEASANT, 1
        RIDER, 2 
        WAGON, 3
        WHEELBARROW, 4
    }
    */

    private void Start() {

    }

    public void PurchaseCourier(int courierType) {

        switch(courierType) {
            case 0: 
                {
                    SpawnController.SpawnPrefab(courierPrefabs[0]);
                    break;
                }
            case 1: 
                {
                    SpawnController.SpawnPrefab(courierPrefabs[1]);
                    break;
                }
            default: 
                {
                    break;
                }
                
                
        }
    }


}
