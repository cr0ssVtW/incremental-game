﻿using UnityEngine;

public class SpawnController : MonoBehaviour {

    static GameObject keepWaypoint;

	// Use this for initialization
	void Start () {
        keepWaypoint = GameObject.FindGameObjectWithTag("WAYPOINT_Keep");
        if (keepWaypoint == null) {
            Debug.LogError("No object found with tag WAYPOINT_Keep");
            enabled = false;
        }
            
	}

    public static void SpawnPrefab(GameObject prefabToSpawn) {
        Vector3 spawnPosition = keepWaypoint.transform.position;
        spawnPosition.y = prefabToSpawn.transform.localScale.y; // TODO: Find a better way to handle this;
        Instantiate(prefabToSpawn, spawnPosition, keepWaypoint.transform.rotation);
    }
}
