﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    int scrollDistance = 5;
    public float dragSpeed = 3f;
    private Vector3 dragOrigin;

    public Camera cameraObject;

    private void Start() {
        cameraObject = GetComponentInChildren<Camera>();
    }

    // Update is called once per frame
    void Update () {

        /** 
         * Keyboard Stuff
        **/

        float translateX = Input.GetAxis("Horizontal");
        float translateY = Input.GetAxis("Vertical");

        transform.Translate(translateX + translateY, 0, translateY - translateX);

        /** 
         * Zoom Stuff
        **/

        if (Input.GetAxis("Mouse ScrollWheel") > 0 && cameraObject.orthographicSize > 4) {
            cameraObject.orthographicSize = cameraObject.orthographicSize - 4;
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0 && cameraObject.orthographicSize < 50) {
            cameraObject.orthographicSize = cameraObject.orthographicSize + 4;
        }

        /** 
         * Mouse Stuff
        **/

        float mousePosX = Input.mousePosition.x;
        float mousePosY = Input.mousePosition.y;
        // Drag move. Middle mouse
        if (Input.GetMouseButtonDown(2)) {
            dragOrigin = Input.mousePosition;
            return;
        }
        if (!Input.GetMouseButton(2)) {
            return;
        }

        Vector3 pos = cameraObject.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        Vector3 move = new Vector3(-pos.x * dragSpeed, 0, -pos.y * dragSpeed);
        transform.Translate(move, Space.World);
    }
}
