﻿using UnityEngine;

public class Item : MonoBehaviour {

    public enum ItemTypes
    {
        ARMOR,
        GEMSTONE,
        JEWELERY,
        ORE,
        PAINTING,
        POTTERY,
        WEAPON,
    }

    int Weight { get; set; }
    int Value { get; set; }
    int Damage { get; set; }
    int ArmorRating { get; set; }

    bool RequiresCart { get; set; }
}
