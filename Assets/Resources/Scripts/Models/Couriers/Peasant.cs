﻿using UnityEngine;

public class Peasant : Courier {

    public string firstName;
    public string lastName;
    private int speedBase = 25;

    public void Start() {
        keepWaypoint = GameObject.FindGameObjectWithTag("WAYPOINT_Keep");
        dungeonWaypoint = GameObject.FindGameObjectWithTag("WAYPOINT_Dungeon");
        firstName = "John";
        lastName = "Doe";
        speed = speedBase + Random.Range(0f, 1f);
        carryCapacity = (int)Random.Range(50, 100);
        armor = 1;
        damage = 2;
    }
}
