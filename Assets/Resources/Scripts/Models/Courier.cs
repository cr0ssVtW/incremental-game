﻿public class Courier : Unit {

    public enum CourierTypes
    {
        CART,
        PEASANT,
        RIDER,
        WAGON,
        WHEELBARROW,
    }

    public int carryCapacity;

    public int armor;
    public int damage;

    public Item[] items;
}
