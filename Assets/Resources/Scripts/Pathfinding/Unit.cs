﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

    public GameObject keepWaypoint;
    public GameObject dungeonWaypoint;
    bool onDelivery;
    bool isMoving;
    Vector3[] path;
    int targetIndex;

    // Default to 2, will be altered by individual units in hierarchy. 
    public float speed = 5f;

    private void Start() {
        onDelivery = false;
        isMoving = true;
    }

    // We want the unit to pingpong back and forth from keep to dungeon
    private void Update() {
        
        if (Vector3.Distance(transform.position, keepWaypoint.transform.position) <= 5) {
            // Reached Keep
            onDelivery = false;
        } else if (Vector3.Distance(transform.position, dungeonWaypoint.transform.position) <= 5) {
            // Reached Dungeon
            onDelivery = true;
        }

        if (onDelivery) {
            // Send to Keep
            PathRequestManager.RequestPath(transform.position, keepWaypoint.transform.position, OnPathFound);
        } else {
            // Send to dungeon
            PathRequestManager.RequestPath(transform.position, dungeonWaypoint.transform.position, OnPathFound);
        }
    }

    public void OnPathFound(Vector3[] newPath, bool pathSuccessful) {
        if (pathSuccessful) {
            path = newPath;
            StopCoroutine("FollowPath");
            StartCoroutine("FollowPath", speed);
            isMoving = true;
        }
    }

    IEnumerator FollowPath(float speed) {
        if (path.Length > 0) {
            Vector3 currentWaypoint = path[0];

            while (true) {
                if (transform.position == currentWaypoint) {
                    targetIndex++;
                    if (targetIndex >= path.Length) {
                        yield break;
                    }
                    currentWaypoint = path[targetIndex];
                }

                transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
                yield return null;
            }
        }
        
    }

    public void OnDrawGizmos() {
        if (path != null) {
            for (int i = targetIndex; i < path.Length; i++) {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[i], Vector3.one);

                if (i == targetIndex) {
                    Gizmos.DrawLine(transform.position, path[i]);
                }
                else {
                    Gizmos.DrawLine(path[i - 1], path[i]);
                }
            }
        }
    }
}
