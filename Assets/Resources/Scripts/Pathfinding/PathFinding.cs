﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;

public class PathFinding : MonoBehaviour {

    PathRequestManager requestManager;
    NodeGrid grid;

    private void Awake() {
        requestManager = GetComponent<PathRequestManager>();
        grid = GetComponent<NodeGrid>();
    }

    public void StartFindPath(Vector3 startPosition, Vector3 targetPosition) {
        StartCoroutine(FindPath(startPosition, targetPosition));
    }

    IEnumerator FindPath(Vector3 startPosition, Vector3 targetPosition) {
        Stopwatch sw = new Stopwatch();
        sw.Start();

        Vector3[] waypoints = new Vector3[0];
        bool pathSuccess = false;

        Node startNode = grid.NodeFromWorldPoint(startPosition);
        Node targetNode = grid.NodeFromWorldPoint(targetPosition);

        if (startNode.walkable && targetNode.walkable) {
            Heap<Node> openSet = new Heap<Node>(grid.MaxSize);
            HashSet<Node> closedSet = new HashSet<Node>();

            openSet.Add(startNode);
            // Find the open node with the lowest F cost
            while (openSet.Count > 0) {
                Node currentNode = openSet.RemoveFirst();
                closedSet.Add(currentNode);

                if (currentNode == targetNode) {
                    sw.Stop();
                    print("Path Found: " + sw.ElapsedMilliseconds + " ms");
                    pathSuccess = true;

                    // found the path
                    break;
                }

                // Loop through neighboring nodes
                foreach (Node neighbor in grid.GetNeighborNodes(currentNode)) {
                    // Check if this neighbor is not walkable or closed, then skip.
                    if (!neighbor.walkable || closedSet.Contains(neighbor)) {
                        continue;
                    }

                    // If the new path to the neighbor node is shorter OR
                    // If the neighbor node is not open, check some stuff then add it to the open. 
                    int newMoveCostToNeighbor = currentNode.gCost + GetDistance(currentNode, neighbor);
                    if (newMoveCostToNeighbor < neighbor.gCost || !openSet.Contains(neighbor)) {
                        neighbor.gCost = newMoveCostToNeighbor;
                        neighbor.hCost = GetDistance(neighbor, targetNode);
                        neighbor.parent = currentNode;

                        if (!openSet.Contains(neighbor)) {
                            openSet.Add(neighbor);
                        }
                        else {
                            openSet.UpdateItem(neighbor);
                        }
                    }
                }
            }
        }
        
        yield return null;
        if (pathSuccess) {
            waypoints = RetracePath(startNode, targetNode);
        }

        requestManager.FinishedProcessingPath(waypoints, pathSuccess);
    }

    Vector3[] RetracePath(Node startNode, Node targetNode) {
        List<Node> path = new List<Node>();
        Node currentNode = targetNode;
        while (currentNode != startNode) {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        Vector3[] waypoints = SimplifyPath(path);
        Array.Reverse(waypoints);

        return waypoints;
    }

    Vector3[] SimplifyPath(List<Node> path) {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 directionOld = Vector2.zero;
        for (int i = 1; i < path.Count; i++) {
            Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
            if (directionNew != directionOld) {
                // changed direction, add a new waypoint
                waypoints.Add(path[i - 1].worldPosition);
            }
            directionOld = directionNew;
        }
        return waypoints.ToArray();
    }

    private int GetDistance(Node nodeA, Node nodeB) {
        // Calculate the X and Y node total by the shortest amount of diagonal
        // nodes to be in line with target node. 
        // Ex: a 6x3 grid with nodeA in bottom left and nodeB in top right
        // O O O O O B
        // O O O O O O 
        // A O O O O O 
        // nodeA must traverse diagonally by two to be in line with nodeB
        // then move over an additional (higherNumber - lowerNumber) to achieve the goal.
        // Thus we get [14*x + 10*(x-y)] if X is larger than Y
        // else [14*y + 10*(y-x)] if Y is larger than X
        int distX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int distY = Mathf.Abs(nodeA.gridY - nodeB.gridY);
        if (distX > distY) {
            return 14 * distY + 10 * (distX - distY);
        }

        return 14 * distY + 10 * (distX - distY);
    }
}
